layui.use(['jquery', 'layer', 'table', 'form'], function() {
	let $ = layui.$;
	let layer = layui.layer;
	let table = layui.table;
	let form = layui.form;

	//数据列表
	if (base.isInRole('job_page')) {
		loadTable()
	}

	function loadTable() {
		let loadIndex = layer.msg('加载中...', {
			icon: 16,
			shade: 0.1
		});
		table.render({
			elem: '#job-table',
			url: base.path + '/jobPage',
			method: 'post',
			where: {
				token: base.getToken()
			},
			done: function(res, curr, count) {
				layer.close(loadIndex);
			},
			cols: [
				[{
						field: 'name',
						title: '任务名',
						width:100
					},
					{
						field: 'groupName',
						title: '任务组',
						width:100
					},
					{
						field: 'className',
						title: '执行类',
						width:200
					},
					{
						field: 'triggerName',
						title: '触发器',
						width:100
					},
					{
						field: 'triggerGroupName',
						title: '触发器组',
						width:100
					},
					{
						field: 'prevFireTime',
						title: '上次执行时间',
						width:200
					},
					{
						field: 'nextFireTime',
						title: '下次执行时间',
						width:200
					},
					{
						field: 'cronExp',
						title: 'cron表达式',
						width:200
					},
					{
						field: 'triggerState',
						title: '触发器状态',
						width:100,
						templet: function(res) {
							return res.triggerState == 'WAITING' ? '等待' : res.triggerState == 'PAUSED' ? '暂停' : res.triggerState == 'ACQUIRED' ? '正常' : res.triggerState == 'BLOCKED' ? '阻塞' : res.triggerState == 'ERROR' ? '错误' : '-';
						}
					},
					{
						title: '操作',
						fixed: 'right',
						align: 'center',
						templet: function(res) {
							let html = '';
							if (base.isInRole('job_pause') && res.triggerState != 'PAUSED') {
								html += "<button lay-event='pause' class='layui-btn layui-btn-sm layui-bg-red'>暂停</button>"
							}
							if (base.isInRole('job_resume') && res.triggerState == 'PAUSED') {
								html += "<button lay-event='resume' class='layui-btn layui-btn-sm layui-bg-blue'>恢复</button>"
							}
							if (base.isInRole('job_update')) {
								html += "<button lay-event='update' class='layui-btn layui-btn-sm layui-bg-green'>编辑</button>"
							}
							if (base.isInRole('job_del')) {
								html += "<button lay-event='del' class='layui-btn layui-btn-sm layui-bg-red'>删除</button>"
							}
							return html;
						}
					}
				]
			],
			limits: [5, 10, 20],
			page: true,
		});
	}


	//查询
	$("#job-search-form .search-btn").click(function() {
		reloadData();
		return false;
	});

	//重新加载
	function reloadData() {
		let loadIndex = layer.msg('加载中...', {
			icon: 16,
			shade: 0.1
		});
		table.reload('job-table', {
			page: {
				curr: 1
			}, //从第一页开始
			where: {
				name: $("#job-search-form [name='name']").val(),
				groupName: $("#job-search-form [name='groupName']").val(),
				token: base.getToken()
			},
			done: function(res, curr, count) {
				layer.close(loadIndex);
			},
		});
	}

	if (!base.isInRole('job_add')) {
		$('#job-add').hide();
	}

	if (!base.isInRole('job_shutdown_all')) {
		$('#job-shutdown-all').hide();
	}

	if (!base.isInRole('job_start_all')) {
		$('#job-start-all').hide();
	}

	//添加用户
	let jobAddLayer;
	$('#job-add').click(function() {
		jobAddLayer = layer.open({
			type: 1,
			title: '添加任务',
			area: ['400px'],
			skin: 'layer-ext-skin',
			content: $('#job-add-dialog'),
			btn: ['提交', '关闭'],
			success: function(index, layero) {
				$("#job-add-form").find("input[name='token']").val(base.getToken());
			},
			yes: function(index, layero) {
				$('#job-add-submit').click();
			}
		});
	});
	//监听form表单提交
	form.on('submit(job-add-submit)', function(data) {
		base.ajax({
			url: '/jobAdd',
			data: $('#job-add-form').serialize(),
			success: function(res) {
				if (res.code == 0) {
					layer.alert('添加成功', function(index) {
						layer.close(index);
						layer.close(jobAddLayer);
						table.reload('job-table');
						$('#job-add-form')[0].reset();
					});
				} else {
					layer.alert(res.msg);
				}
			}
		});
		return false;
	});

	$('#job-shutdown-all').click(function() {
		layer.confirm('确定要停止所有任务吗？', {
			icon: 2,
			title: '提示'
		}, function(index) {
			base.ajax({
				url: '/jobShutduwnAll',
				success: function(res) {
					if (res.code == 0) {
						layer.alert('停止成功', function(index2) {
							layer.close(index);
							layer.close(index2);
							table.reload('job-table');
						});
					} else {
						layer.alert(res.msg);
					}
				}
			});
		});
	});

	$('#job-start-all').click(function() {
		layer.confirm('确定要启动所有任务吗？', {
			icon: 1,
			title: '提示'
		}, function(index) {
			base.ajax({
				url: '/jobStartAll',
				success: function(res) {
					if (res.code == 0) {
						layer.alert('启动成功', function(index2) {
							layer.close(index);
							layer.close(index2);
							table.reload('job-table');
						});
					} else {
						layer.alert(res.msg);
					}
				}
			});
		});
	});

	//监听工具栏事件
	table.on('tool(job-table)', function(obj) {
		let d = obj.data;
		switch (obj.event) {
			case 'pause':
				optJob(d.name, d.groupName, 1);
				break;
			case 'resume':
				optJob(d.name, d.groupName, 2);
				break;
			case 'update':
				$("#job-edit-form").find("input[name='token']").val(base.getToken());
				$("#job-edit-form").find("input[name='name']").val(d.name);
				$("#job-edit-form").find("input[name='groupName']").val(d.groupName);
				$("#job-edit-form").find("input[name='cronExp']").val(d.cronExp);
				layui.form.render('select'); //刷新select下拉框
				editjob();
				break;
			case 'del':
				optJob(d.name, d.groupName, 3);
				break;
		}
	});



	function optJob(name, groupName, option) {
		let url = option == 1 ? '/jobPause' : option == 2 ? '/jobResume' : '/jobDel';
		let txt = option == 1 ? '暂停' : option == 2 ? '恢复' : '删除';
		layer.confirm("确认要" + txt + "[ " + name + " ]任务吗？", function(index2) {
			layer.close(index2);
			base.ajax({
				url: url,
				data: {
					'name': name,
					'groupName': groupName,
					token: base.getToken()
				},
				success: function(res) {
					if (res.code == 0) {
						layer.alert(txt + '成功', function(index) {
							layer.close(index);
							table.reload('job-table');
						});
					} else {
						layer.alert(res.msg);
					}
				}
			});
		});
	}


	//编辑任务
	function editjob() {
		layer.open({
			type: 1,
			title: '编辑任务',
			area: ['400px'],
			skin: 'layer-ext-skin',
			content: $('#job-edit-dialog'),
			btn: ['提交', '关闭'],
			yes: function(index, layero) {
				base.ajax({
					url: '/jobUpdate',
					data: $('#job-edit-form').serialize(),
					success: function(res) {
						if (res.code == 0) {
							layer.alert('修改成功', function(index2) {
								layer.close(index);
								layer.close(index2);
								table.reload('job-table');
							});
						} else {
							layer.alert(res.msg);
						}
					}
				});
			}
		});
	}


});
