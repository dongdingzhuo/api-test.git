layui.use(['jquery', 'layer', 'table', 'laydate', 'upload', 'form'], function() {
	let $ = layui.$;
	let layer = layui.layer;
	let table = layui.table;
	let laydate = layui.laydate;
	let upload = layui.upload;
	let form = layui.form;

	//日期时间选择器
	laydate.render({
		elem: '#startDay',
		format: 'yyyy-MM-dd'
	});
	//日期时间选择器
	laydate.render({
		elem: '#endDay',
		format: 'yyyy-MM-dd'
	});

	upload.render({
		elem: '#file-add-form-file',
		field: 'file',
		auto: false // 选择文件后不自动上传
			,
		accept: 'file' // 可选值有：images（图片）、file（所有文件）、video（视频）、audio（音频）
	});

	//查询
	$("#file-search-form .search-btn").click(function() {
		reloadData();
		return false;
	});

	//数据列表
	if (base.isInRole('file_page')) {
		loadTable();
	}

	function loadTable() {
		let loadIndex = layer.msg('加载中...', {
			icon: 16,
			shade: 0.1
		});
		table.render({
			elem: '#file-table',
			url: base.path + '/filePage',
			where: {
				token: base.getToken()
			},
			method: 'post',
			done: function(res, curr, count) {
				layer.close(loadIndex);
			},
			cols: [
				[{
						field: 'realName',
						title: '文件真实名称'
					},
					{
						field: 'name',
						title: '系统命名'
					},
					{
						field: 'system',
						title: '所属系统'
					},
					{
						field: 'type',
						title: '所属类型'
					},
					{
						field: 'uri',
						title: '存放地址'
					},
					{
						field: 'md5',
						title: '加密指纹'
					},
					{
						field: 'size',
						title: '文件大小',
						templet: function(res) {
							return res.size < 1024 ? res.size + 'bit' : (res.size < 1024 * 1024 ? (res.size / 1024).toFixed(2) + 'KB' : (res.size / (1024 * 1024)).toFixed(2) + 'MB');
						}
					},
					{
						field: 'contentType',
						title: '文件类型'
					},
					{
						field: 'createTime',
						title: '访问时间'
					}
				]
			],
			limits: [5, 10, 20],
			page: true
		});
	}

	//重新加载
	function reloadData() {
		let loadIndex = layer.msg('加载中...', {
			icon: 16,
			shade: 0.1
		});
		table.reload('file-table', {
			page: {
				curr: 1
			}, //从第一页开始
			where: {
				system: $("#file-search-form [name='system']").val(),
				startDay: $("#file-search-form [name='startDay']").val(),
				endDay: $("#file-search-form [name='endDay']").val(),
				token: base.getToken()
			},
			done: function(res, curr, count) {
				layer.close(loadIndex);
			},
		});
	}

	if (!base.isInRole('file_upload')) {
		$('#file-upload').hide();
	}

	//上传文件
	let fileAddLayer;
	$('#file-upload').click(function() {
		fileAddLayer = layer.open({
			type: 1,
			title: '上传文件',
			area: ['400px'],
			skin: 'layer-ext-skin',
			content: $('#file-add-dialog'),
			btn: ['提交', '关闭'],
			success: function(index, layero) {
				$("#file-add-form").find("input[name='token']").val(base.getToken());
			},
			yes: function(index, layero) {
				$('#file-add-submit').click();
			}
		});
	});
	//监听form表单提交
	form.on('submit(file-add-submit)', function(data) {
		var fd = new FormData(document.getElementById("file-add-form"));
		base.ajax({
			url: '/fileUpload',
			data: fd,
			contentType: false,
			processData: false,
			success: function(res) {
				if (res.code == 0) {
					//todo:会返回文件上传后的地址
					layer.alert('上传成功', function(index) {
						layer.close(index);
						layer.close(fileAddLayer);
						table.reload('file-table');
						$('#file-add-form')[0].reset();
					});
				} else {
					layer.alert(res.msg);
				}
			}
		});
		return false;
	});

});
