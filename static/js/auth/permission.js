layui.use(['jquery', 'layer', 'table'], function() {
	let $ = layui.$;
	let layer = layui.layer;
	let table = layui.table;

	//查询
	$("#permission-search .search-btn").click(function() {
		reloadData();
		return false;
	});

	//数据列表
	if (base.isInRole('permission_page')) {
		loadTable()
	}

	function loadTable() {
		let loadIndex = layer.msg('加载中...', {
			icon: 16,
			shade: 0.1
		});
		table.render({
			elem: '#permission-table',
			url: base.path + '/permissionPage',
			where: {
				token: base.getToken()
			},
			method: 'post',
			done: function(res, curr, count) {
				layer.close(loadIndex);
			},
			cols: [
				[{
						field: 'name',
						title: '权限名称'
					},
					{
						field: 'code',
						title: '权限代码',
						sort: true
					},
					{
						field: 'url',
						title: '权限地址',
						sort: true
					},
					{
						field: 'menu',
						title: '权限类型',
						width: 100,
						sort: true,
						templet: function(res) {
							return res.menu == 1 ? "菜单" : "其他";
						}
					},
					{
						field: 'parentName',
						title: '父级权限'
					},
					{
						field: 'orderView',
						title: '排序号',
						width: 100,
						sort: true
					},
					{
						field: 'state',
						title: '权限状态',
						width: 100,
						templet: function(res) {
							return res.state == 1 ? "<span style='color:#1E9FFF'>有效</span>" : "<span style='color:#FF5722'>无效</span>";
						}
					}
				]
			],
			limits: [5, 10, 20],
			page: true
		});
	}

	//重新加载
	function reloadData() {
		let loadIndex = layer.msg('加载中...', {
			icon: 16,
			shade: 0.1
		});
		table.reload('permission-table', {
			page: {
				curr: 1
			}, //从第一页开始
			where: {
				name: $("#permission-search [name='name']").val(),
				code: $("#permission-search [name='code']").val(),
				menu: $("#permission-search [name='menu']").val(),
				state: $("#permission-search [name='state']").val(),
				token: base.getToken()
			},
			done: function(res, curr, count) {
				layer.close(loadIndex);
			},
		});
	}

});
