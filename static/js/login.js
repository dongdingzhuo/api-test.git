layui.use(['jquery', 'layer'], function () {
    let $ = layui.$;
    let layer = layui.layer;

    //input样式控制
    $('#account, #password').focus(function () {
        $(this).parent().addClass('field-focus');
    });
    $('#account, #password').blur(function (e) {
        if (!e.target.value) $(this).parent().removeClass('field-focus');
    });

    //点击登录
    $('#login_btn').click(function () {
        login();
    });
    //回车事件登录
    $('#account, #password').keypress(function (event) {
        if (event.keyCode == 13) {
            login();
        }
    });

    function login() {
        base.ajax({
            url: '/login',
            data: $('.login-form').serialize(),
            success: function (res) {
                if (res.code != 0) {
                    layer.alert(res.msg);
                    return;
                }
                let d = res.data;
                sessionStorage.setItem('account', d.account);
                sessionStorage.setItem('token', d.token);
                sessionStorage.setItem('name', d.name);
                sessionStorage.setItem('userId', d.id);
                sessionStorage.setItem('menu', JSON.stringify(d.menu));
                sessionStorage.setItem('permissionCodes', JSON.stringify(d.permissionCodes));
                window.location.href = '/api-test/index.html';
            }
        });
    }

});