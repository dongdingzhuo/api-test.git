layui.use(['jquery', 'layer', 'table', 'laydate'], function() {
	let $ = layui.$;
	let layer = layui.layer;
	let table = layui.table;
	let laydate = layui.laydate;

	//日期时间选择器
	laydate.render({
		elem: '#startDay',
		format: 'yyyy-MM-dd'
	});
	//日期时间选择器
	laydate.render({
		elem: '#endDay',
		format: 'yyyy-MM-dd'
	});

	//查询
	$("#log-user-search .search-btn").click(function() {
		reloadData();
		return false;
	});

	//数据列表
	if (base.isInRole('log_user_page')) {
		loadTable()
	}

	function loadTable() {
		let loadIndex = layer.msg('加载中...', {
			icon: 16,
			shade: 0.1
		});
		table.render({
			elem: '#log-user-table',
			url: base.path + '/logUser/page',
			where: {
				token: base.getToken()
			},
			method: 'post',
			done: function(res, curr, count) {
				layer.close(loadIndex);
			},
			cols: [
				[{
						field: 'userName',
						title: '访问人'
					},
					{
						field: 'uri',
						title: '访问路径'
					},
					{
						field: 'param',
						title: '携带参数'
					},
					{
						field: 'createTime',
						title: '访问时间'
					}
				]
			],
			limits: [5, 10, 20],
			page: true
		});
	}

	//重新加载
	function reloadData() {
		let loadIndex = layer.msg('加载中...', {
			icon: 16,
			shade: 0.1
		});
		table.reload('log-user-table', {
			page: {
				curr: 1
			}, //从第一页开始
			where: {
				uri: $("#log-user-search [name='uri']").val(),
				startDay: $("#log-user-search [name='startDay']").val(),
				endDay: $("#log-user-search [name='endDay']").val(),
				token: base.getToken()
			},
			done: function(res, curr, count) {
				layer.close(loadIndex);
			},
		});
	}

});
