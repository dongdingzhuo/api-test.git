var base;
layui.use(['jquery', 'layer'], function() {
	var $ = layui.$,
		layer = layui.layer;

	base = {
		path: 'http://localhost:9999',
		isEmpty: function(str) {
			if (null == str || undefined == str || 'undefined' == str || 'null' == str || $.trim(str).length <= 0) {
				return true;
			}
			return false;
		},
		isNotEmpty: function(str) {
			return !base.isEmpty(str);
		},
		arrayFilter: function(arr) {
			return arr.filter(function(v) {
				return !(!v || v === "" || v.length == 0);
			});
		},
		isInRole: function(str) {
			let arr = JSON.parse(sessionStorage.getItem('permissionCodes'));
			for (let i = 0; i < arr.length; i++) {
				if (str === arr[i]) {
					return true;
				}
			}
			return false;
		},
		getToken: function() {
			let token = sessionStorage.getItem('token');
			return token;
		},
		getJson: function(str) {
			if (typeof str == 'string') {
				try {
					let json = eval('(' + str + ')');
					if (typeof json == 'object' && json) {
						return json;
					}
				} catch (e) {}
			}
			return null;
		},
		previewImg: function(t) { //图片上传预览
			let file = t.files[0];
			$(t).parent().prev().attr('src', getObjectURL(file));

			function getObjectURL(file) {
				let url = null;
				if (window.createObjectURL != undefined) { //base
					url = window.createObjectURL(file);
				} else if (window.URL != undefined) { //mozilla
					url = window.URL.createObjectURL(file);
				} else if (window.webkitURL != undefined) { //webkit
					url = window.webkitURL.createObjectURL(file);
				}
				return url;
			}
		},
		fullImg: function(t) { //查看大图
			let container = document.createElement('div');
			let span = document.createElement('span');
			span.innerText = '×';
			container.classList.add('fullImg');
			let img = new Image();
			img.src = t.src;
			container.appendChild(img);
			container.appendChild(span);
			container.style.display = 'block';
			document.body.appendChild(container);
			container.onclick = function() {
				document.body.removeChild(container);
			}
		},
		showLoading: function(msg) {
			if ($(".loading").length > 0) {
				$(".loading").remove();
			}
			$('body').append('<div class="loading"><div class="loading-panel"><img src="/common/images/loading.gif"><p>' +
				msg + '</p></div></div>');
			let maskWidth = parseInt($(".loading-panel").width());
			let widthHalf = -maskWidth / 2 + 'px';
			$(".loading-panel").css({
				"margin-left": widthHalf
			});
		},
		hideLoading: function() {
			$(".loading").remove();
		},
		textWidth: function(obj) {
			let currentObj = $('<pre>').hide().appendTo(document.body);
			$(currentObj).html(obj.text()).css('font', obj.css('font'));
			let width = currentObj.width();
			currentObj.remove();
			return width;
		},
		ts2date: function(ts) {
			let dt = new Date(ts);
			let year = dt.getFullYear();
			let month = dt.getMonth() + 1;
			month = month < 10 ? ('0' + month) : month;
			let date = dt.getDate();
			date = date < 10 ? ('0' + date) : date;
			return year + '-' + month + '-' + date;
		},
		ts2time: function(ts) {
			let dt = new Date(ts);
			let hour = dt.getHours();
			hour = hour < 10 ? ('0' + hour) : hour;
			let minute = dt.getMinutes();
			minute = minute < 10 ? ('0' + minute) : minute;
			let second = dt.getSeconds();
			second = second < 10 ? ('0' + second) : second;
			return hour + ':' + minute + ':' + second;
		},
		ts2datetime: function(ts) {
			return base.ts2date(ts) + ' ' + base.ts2time(ts);
		},
		second2str: function(seconds) {
			let str = 0;
			if (seconds < 60) {
				str = seconds + "s"
			} else if (seconds < 3600) {
				let minutes = parseInt(seconds / 60 + "");
				let second = seconds % 60;
				str = minutes + "m" + second + "s";
			} else if (seconds < 86400) {
				let hours = parseInt(seconds / 3600 + "");
				let overSeconds = seconds - hours * 3600;
				let minutes = parseInt(overSeconds / 60 + "");
				let second = overSeconds % 60;
				str = hours + "h" + minutes + "m" + second + "s";
			} else {
				let day = parseInt(seconds / 86400 + "");
				let overSeconds = seconds - day * 86400;
				let hours = parseInt(overSeconds / 3600 + "");
				overSeconds = overSeconds - hours * 3600;
				let minutes = parseInt(overSeconds / 60 + "");
				let second = overSeconds % 60;
				str = day + "d" + hours + "h" + minutes + "m" + second + "s";
			}
			return str;
		},
		currentMonth: function() {
			let dt = new Date();
			let year = dt.getFullYear();
			let month = dt.getMonth() + 1;
			month = month < 10 ? ('0' + month) : month;
			return year + "-" + month + "-" + "01";
		},
		nextMonth: function() {
			let dt = new Date();
			let year = dt.getFullYear();
			let month = dt.getMonth() + 2;
			if (month > 12) {
				year = year + 1;
				month = 1;
			}
			month = month < 10 ? ('0' + month) : month;
			return year + "-" + month + "-" + "01";
		},
		fitScreen: function(tagId) {
			let searchHeight = $('#' + tagId + '-search').innerHeight();
			let bodyHeight = $(document.body).height();
			$('#' + tagId + '-data').height((bodyHeight - searchHeight - 141) + "px");
		},
		serialize2json: function(form) {
			let json = {};
			let array = form.serializeArray();
			$(array).each(function() {
				if (json[this.name]) {
					if ($.isArray(json[this.name])) {
						json[this.name].push(this.value);
					} else {
						json[this.name] = [json[this.name], this.value];
					}
				} else {
					json[this.name] = this.value;
				}
			});
			return json;
		},
		ajax: function(obj) {
			let param = {},
				index;
			if (base.isEmpty(obj.url)) {
				console.log('url不能为空');
				return;
			}
			param.url = base.path + obj.url;
			if (base.isNotEmpty(obj.data)) {
				param.data = obj.data;
			}
			param.type = base.isEmpty(obj.type) ? 'post' : obj.type;
			param.timeout = base.isEmpty(obj.timeout) ? 60000 : obj.timeout;
			param.beforeSend = base.isEmpty(obj.beforeSend) ? function() {
				index = layer.load(2, {
					shade: [0.1, '#000']
				})
			} : obj.beforeSend;
			param.complete = base.isEmpty(obj.complete) ? function(jqXHR, textStatus) {
				layer.close(index);
				let json = base.getJson(jqXHR.responseText);
				if (json != null && json.code != undefined) {
					if (json.code == 401 || json.code == 403) {
						layer.alert(json.msg, function() {
							sessionStorage.clear();
							window.top.location.href = "/api-test/page/login.html";
						});
					}
				}
			} : obj.complete;
			param.dataType = base.isEmpty(obj.dataType) ? 'json' : obj.dataType;
			param.async = base.isEmpty(obj.async) ? true : obj.async;
			param.processData = base.isEmpty(obj.processData) ? true : obj.processData;
			param.contentType = base.isEmpty(obj.contentType) ? "application/x-www-form-urlencoded" : obj.contentType;
			param.statusCode = {
				statusCode: {
					400: function() {
						alert('400：请求参数错误');
					},
					404: function() {
						alert('404：请求地址错误');
					},
					405: function() {
						alert('405：请求GET/POST错误');
					},
					500: function() {
						alert('500：服务器错误');
					}
				}
			};
			param.success = obj.success;
			param.success = function(data, textStatus, jqXHR) {
				obj.success(data, textStatus, jqXHR);
			};
			param.error = function(XMLHttpRequest, textStatus, errorThrown) {
				if (textStatus == 'timeout') {
					alert('请求超时,请重试');
				}
			};
			$.ajax(param);
		}
	};
});